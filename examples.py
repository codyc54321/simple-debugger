#!/usr/bin/env python

from simple_debugger import debugger

""" run me!

      python examples.py
"""

class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'


THESE_BLOCKS = []

THESE_BLOCKS.append("""
@debugger
def one_two(one, two):
    pass

one_two(1, 2)
""")

THESE_BLOCKS.append("""
@debugger
def one_two_three_silent(one, two, three=3):
    pass

one_two_three_silent(1, 2)
""")

THESE_BLOCKS.append("""
@debugger
def kwarg_one_kwarg_two(one=None, two=None):
    pass

kwarg_one_kwarg_two(1, 2)
""")

THESE_BLOCKS.append("""
@debugger
def one_two_kwarg_three(one, two, three=None):
    pass

one_two_kwarg_three(1, 2, 3)
""")

THESE_BLOCKS.append("""
@debugger
def one_two_kwarg_three_star_args_star_kwargs(one, two, three=None,  *args, **kwargs):
    pass

one_two_kwarg_three_star_args_star_kwargs(1, 2, 3, 'star arg 1', 'star arg 2', blah='blah, kwarg')
""")

THESE_BLOCKS.append("""
@debugger
def star_args_star_kwargs(*args, **kwargs):
    pass

star_args_star_kwargs('arg 1', 'arg 2', star_arg_1='star arg 1', star_arg_2='star arg 2')
""")


def print_code_example(codeblock):
    print(bcolors.WARNING + "\nBased on code:" + bcolors.ENDC)
    print(codeblock)
    print(bcolors.WARNING + "It prints out:" + bcolors.ENDC)
    exec(codeblock)
    print("-----------------------------")

for block in THESE_BLOCKS:
    print_code_example(block)
