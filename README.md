
```
pip install simple-debugger
```

Then

```
from simple_debugger import debugger

@debugger
def build_river_empire(beaver_count, runnel_location, front_teeth_length=10):
    # now do the dam thing
    pass
```

### simple-debugger is...

*Lean.*

    @debugger
    def one_two(one, two):
        pass

    one_two(1, 2)

prints out:

    debugging 'one_two':

        args:

            one = 1
            two = 2

### simple-debugger is...

*Bossin'.*

    @debugger
    def one_two_three_silent(one, two, three=3):
        pass

    one_two_three_silent(1, 2)

prints out:

    debugging 'one_two_three_silent':

        args:

            one = 1
            two = 2

        **kwargs:

            three = 3

### simple-debugger is...

*Clean.*

    @debugger
    def kwarg_one_kwarg_two(one=None, two=None):
        pass

    kwarg_one_kwarg_two(1, 2)

prints out:

    debugging 'kwarg_one_kwarg_two':

        args:

            one = 1
            two = 2

### simple-debugger is...

*Flossin'.*

    @debugger
    def one_two_kwarg_three(one, two, three=None):
        pass

    one_two_kwarg_three(1, 2, 3)

prints out:

    debugging 'one_two_kwarg_three':

        args:

            one = 1
            two = 2
            three = 3

### simple-debugger is...

*PRISTINE.*

    @debugger
    def one_two_kwarg_three_star_args_star_kwargs(one, two, three=None,  *args, **kwargs):
        pass

    one_two_kwarg_three_star_args_star_kwargs(1, 2, 3, 'star arg 1', 'star arg 2', blah='blah, kwarg')

prints out:

    debugging 'one_two_kwarg_three_star_args_star_kwargs':

        args:

            one = 1
            two = 2
            three = 3

        *args:

            "star arg 1"
            "star arg 2"

        **kwargs:

            blah = "blah, kwarg"

### simple-debugger is...

**AWESOME.**

    @debugger
    def star_args_star_kwargs(*args, **kwargs):
        pass

    star_args_star_kwargs('arg 1', 'arg 2', star_arg_1='star arg 1', star_arg_2='star arg 2')

prints out:

    debugging 'star_args_star_kwargs':

        *args:

            "arg 1"
            "arg 2"

        **kwargs:

            star_arg_2 = "star arg 2"
            star_arg_1 = "star arg 1"
