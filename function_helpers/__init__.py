
def one_two(one, two):
    pass

def one_two_three_silent(one, two, three=3):
    pass

def kwarg_one_kwarg_two(one=None, two=None):
    pass

def one_two_kwarg_three(one, two, three=None):
    pass

def one_two_kwarg_three_star_args_star_kwargs(one, two, three=None,  *args, **kwargs):
    pass

def star_args_star_kwargs(*args, **kwargs):
    pass
