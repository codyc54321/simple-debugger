from setuptools import setup


setup(name='simple-debugger',
      description='A simple debugger to watch args/kwargs flowing through your functions',
      version='0.2.0',
      url='https://bitbucket.org/codyc54321/simple-debugger',
      author='Cody Childers',
      author_email='cchilder@mail.usf.edu',
      license='MIT',
      classifiers=[
          'Programming Language :: Python :: 3'
      ],
      packages=['simple_debugger'],
      install_requires=[ ],
      entry_points={ }
)
