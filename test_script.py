#!/usr/bin/env python

from simple_debugger import debugger

from function_helpers import (
    one_two,
    one_two_three_silent,
    kwarg_one_kwarg_two,
    one_two_kwarg_three,
    one_two_kwarg_three_star_args_star_kwargs,
    star_args_star_kwargs
)

one_two = debugger(one_two)
one_two_three_silent = debugger(one_two_three_silent)
kwarg_one_kwarg_two = debugger(kwarg_one_kwarg_two)
one_two_kwarg_three = debugger(one_two_kwarg_three)
one_two_kwarg_three_star_args_star_kwargs = debugger(one_two_kwarg_three_star_args_star_kwargs)
star_args_star_kwargs = debugger(star_args_star_kwargs)

one_two(1, 2)
one_two_three_silent(1, 2)
kwarg_one_kwarg_two(1, 2)
one_two_kwarg_three(1, 2, 3)
one_two_kwarg_three_star_args_star_kwargs(1, 2, 3, 'star arg 1', 'star arg 2', blah='blah, kwarg')
star_args_star_kwargs('arg 1', 'arg 2', star_arg_1='star arg 1', star_arg_2='star arg 2')
