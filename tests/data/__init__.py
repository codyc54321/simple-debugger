from function_helpers import (
    one_two,
    one_two_three_silent,
    kwarg_one_kwarg_two,
    one_two_kwarg_three,
    one_two_kwarg_three_star_args_star_kwargs,
    star_args_star_kwargs
)


ONE_TWO_EXPECTATION = """
debugging 'one_two':

    args:

        one = 1
        two = 2
"""

ONE_TWO_THREE_SILENT_EXPECTATION = """
debugging 'one_two_three_silent':

    args:

        one = 1
        two = 2

    **kwargs:

        three = 3
"""

KWARG_ONE_KWARG_TWO_EXPECTATION = """
debugging 'kwarg_one_kwarg_two':

    args:

        one = 1
        two = 2
"""

ONE_TWO_KWARG_THREE_EXPECTATION = """
debugging 'one_two_kwarg_three':

    args:

        one = 1
        two = 2
        three = 3
"""

ONE_TWO_KWARG_THREE_STAR_ARGS_STAR_KWARGS_EXPECTATION = """
debugging 'one_two_kwarg_three_star_args_star_kwargs':

    args:

        one = 1
        two = 2
        three = 3

    *args:

        "star arg 1"
        "star arg 2"

    **kwargs:

        blah = "blah, kwarg"
"""

STAR_ARGS_STAR_KWARGS_EXPECTATION = """
debugging 'star_args_star_kwargs':

    *args:

        "arg 1"
        "arg 2"

    **kwargs:

        star_arg_2 = "star arg 2"
        star_arg_1 = "star arg 1"
"""

TEST_GENERATE_DEBUG_STATEMENT_MAP = [
    {
        'callback': one_two,
        'expectation': ONE_TWO_EXPECTATION,
        'args': (1, 2)
    },
    {
        'callback': one_two_three_silent,
        'expectation': ONE_TWO_THREE_SILENT_EXPECTATION,
        'args': (1, 2)
    },
    {
        'callback': kwarg_one_kwarg_two,
        'expectation': KWARG_ONE_KWARG_TWO_EXPECTATION,
        'args': (1, 2)
    },
    {
        'callback': one_two_kwarg_three,
        'expectation': ONE_TWO_KWARG_THREE_EXPECTATION,
        'args': (1, 2, 3)
    },
    {
        'callback': one_two_kwarg_three_star_args_star_kwargs,
        'expectation': ONE_TWO_KWARG_THREE_STAR_ARGS_STAR_KWARGS_EXPECTATION,
        'args': (1, 2, 3, 'star arg 1', 'star arg 2'),
        'kwargs': {'blah': 'blah, kwarg'}
    },
    {
        'callback': star_args_star_kwargs,
        'expectation': STAR_ARGS_STAR_KWARGS_EXPECTATION,
        'args': ('arg 1', 'arg 2'),
        'kwargs': {'star_arg_1': 'star arg 1', 'star_arg_2': 'star arg 2'}
    },
]
