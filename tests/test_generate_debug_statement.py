import unittest

from simple_debugger import generate_debug_statement
from tests.data import TEST_GENERATE_DEBUG_STATEMENT_MAP


class TestGenerateDebugStatement(unittest.TestCase):

    def test_all(self):
        self.run_tests()

    def run_tests(self):
        for item in TEST_GENERATE_DEBUG_STATEMENT_MAP:
            args = item.get('args', ())
            kwargs = item.get('kwargs', {})
            result = generate_debug_statement(item['callback'], args=args, kwargs=kwargs)
            self.assertEqual(result, item['expectation'])
